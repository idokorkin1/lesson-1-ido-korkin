#include <iostream>
#include "stack.h"
#include "utils.h"


void push(stack* s, unsigned int element)
{
	stack* curr = s;
	stack* new_node = new stack;
	while (curr->next)
	{
		curr = curr->next;
	}
	new_node->num = element;
	new_node->next = NULL;
	curr->next = new_node;
}

int pop(stack* s)
{
	int num = 0;
	if (s->next == NULL)
	{
		return -1;
	}
	else
	{
		stack* curr = s;
		while (curr->next->next)
		{
			curr = curr->next;
		}
		num = curr->next->num;
		delete[] curr->next;
		curr->next = NULL;
		return num;
	}
}

void initStack(stack* s)
{
	s->next = 0;
}

void cleanStack(stack* s)
{
	stack* curr = s;
	stack* temp = NULL;
	while (curr != NULL)
	{
		temp = curr;
		curr = curr->next;
		delete[] temp;
	}
}
