#include <iostream>
#include "stack.h"
#include "utils.h"


void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = new stack;
	initStack(s);
	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
}

int* reverse10()
{
	int i = 0;
	int* nums = new int[10];
	for (i = 0; i < 10; i++)
	{
		std::cin >> nums[i];
	}
	reverse(nums, 10);

	return nums;
}