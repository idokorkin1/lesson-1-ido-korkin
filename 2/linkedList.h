#include <iostream>


typedef struct stack {
	int element;
	struct stack* next;
}stack;

stack* checkNullBefore(stack* first, unsigned int num);
stack* initStack(stack* first, unsigned int num);
void cleanTheStack(stack* first);
void reverseList(stack** first);