#include <iostream>
#include "linkedList.h"



stack* checkNullBefore(stack* first, unsigned int num)
{
	stack* curr = first;

	if (curr)
	{
		while (curr->next)
		{
			curr = curr->next;
		}
		curr->next = initStack(curr->next,num);
	}
	else
	{
		first = initStack(first, num);
	}
	return first;
}

stack* initStack(stack* first, unsigned int num)
{
	stack* curr = first;
	curr = new stack;
	curr->element = num;
	curr->next = NULL;
	return curr;
}

void cleanTheStack(stack* first)
{
	stack* curr = first;
	while (curr->next->next)
	{
		curr = curr->next;
	}
	delete[] curr->next;
	curr->next = NULL;
}