#include <iostream>
#include "queue.h"

void initQueue(queue* q, unsigned int size)
{
	int i = 0;
	q->elements = new int[size];
	q->count = 0;
	q->maxSize = size;
}

void cleanQueue(queue* q)
{
	delete[] &q->elements;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->maxSize == q->count)
	{
		std::cout << "you can't add more elements" << std::endl;
	}
	q->elements[q->count] = newValue;
	q->count += 1;
}

int dequeue(queue* q)
{
	if (q->count == 0)
	{
		return -1;
	}
	else
	{
		int num = q->elements[0];
		q->elements = q->elements + 1;
		q->count -= 1;
		return num;
	} 
}
